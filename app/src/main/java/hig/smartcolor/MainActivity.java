package hig.smartcolor;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;


/**
 * Main activity for the app. At the moment everything happens
 * in a single place, and could be improved and refactored.
 *
 * Created by mariusz on 7/07/15.
 */
public class MainActivity extends Activity {

    public final static int PORT = 9000;

    // What mode are we in?
    boolean mInClientMode;

    // Are we connected?
    boolean mIsConnected = false;


    // WiFi management
    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;

    // UI elements
    Button btnClientMode;
    Button btnServerMode;
    TextView txtModeSelection;


    // Socket connectivity
    Socket mSocket;
    OutputStreamWriter mOutput;
    BufferedReader mInput;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupUI();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Called when Client Mode button is pressed.
     * @param button client mode button.
     */
    public void onClientMode(final View button) {
        Toast.makeText(this, "Client Mode ON", Toast.LENGTH_SHORT).show();
        this.mInClientMode = true;
        hidePrompt("Client Mode");
        setupWifiDirect(false);
        discoverPeers();
    }


    /**
     * Called when Server Mode button is pressed.
     * @param button server mode button
     */
    public void onServerMode(final View button) {
        Toast.makeText(this, "Server Mode ON", Toast.LENGTH_SHORT).show();
        this.mInClientMode = false;
        hidePrompt("Server Mode");
        setupServer();
    }


    /**
     * Called when new P2P devices become available
     * @param devices
     */
    public void onAvailableP2pDevices(final WifiP2pDevice[] devices) {
        Log.i("SmartColor", "Number of peers discovered: " + devices.length);
        if (mIsConnected) {
            // if we are already connected, we do nothing
            return;
        }

        if (devices.length < 1) {
            // sanity check
            return;
        }

        if (mInClientMode) {
            // let us try to connect to the server
            new Thread() {
                @Override
                public void run() {
                    connectToServer(devices[0]);
                }
            }.start();
        }
    }

    private void setupWifiDirect(final boolean isServer) {
        // necessary setup for P2P mode
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        WifiP2pConfig config = new WifiP2pConfig();
        if (isServer) config.groupOwnerIntent = 15;

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
    }

    private void discoverPeers() {
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Toast.makeText(MainActivity.this, "Success: FOUND PEERS", Toast.LENGTH_SHORT).show();
                mManager.requestPeers(mChannel, new WifiP2pManager.PeerListListener() {
                    @Override
                    public void onPeersAvailable(WifiP2pDeviceList peers) {
                        MainActivity.this.onAvailableP2pDevices(
                                peers.getDeviceList().toArray(new WifiP2pDevice[peers.getDeviceList().size()]));
                    }
                });
            }

            @Override
            public void onFailure(int reasonCode) {
                Toast.makeText(MainActivity.this, "Failure: NO PEERS", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Setup server socket to listen for peers.
     */
    private void setupServer() {
        setupWifiDirect(true);
        new Thread() {
            public void run() {
                try {
                    final ServerSocket serverSocket = new ServerSocket(PORT);
                    Log.i("Smart Color", "Ready to listen for clients");
                    final Socket client = serverSocket.accept();
                    Log.i("Smart Color", "Client connected");
                    final OutputStreamWriter out = new OutputStreamWriter(client.getOutputStream());
                    final BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    String line = in.readLine();
                    while (line != null) {
                        out.write(line + " .. from server\n");
                        out.flush();
                        Log.i("Smart Color", "Got: " + line);
                        line = in.readLine();
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * Connect to the server
     * @param device the server device
     */
    private void connectToServer(WifiP2pDevice device) {
        Log.i("Smart Color", "Connecting to server");
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.groupOwnerIntent = 0;
        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                mManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                    @Override
                    public void onConnectionInfoAvailable(final WifiP2pInfo wifiP2pInfo) {
                        new Thread() {
                            @Override
                            public void run() {
                                final InetAddress address = wifiP2pInfo.groupOwnerAddress;
                                mSocket = new Socket();
                                try {
                                    mSocket.connect((new InetSocketAddress(address, PORT)), 2000);
                                    mOutput = new OutputStreamWriter(mSocket.getOutputStream());
                                    mOutput.write("Hello\n");
                                    mOutput.flush();
                                    mInput = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
                                    String response = mInput.readLine();
                                    Log.i("Smart Color", "GOT response from server;" + response);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                    }
                });
            }

            @Override
            public void onFailure(int reason) {
                //failure logic
            }
        });
    }


    private void shutdownCommunication() {
        try {
            if (mInput != null) mInput.close();
            if (mOutput != null) mOutput.close();
            if (mSocket != null) mSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mIsConnected = false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mReceiver != null) {
            registerReceiver(mReceiver, mIntentFilter);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }

    @Override
    protected void onDestroy() {
        shutdownCommunication();
        super.onDestroy();
    }


    private void setupUI() {
        btnClientMode = (Button) findViewById(R.id.btn_client_mode);
        btnServerMode = (Button) findViewById(R.id.btn_server_mode);
        txtModeSelection = (TextView) findViewById(R.id.prompt);
    }

    private void hidePrompt(final String mode) {
        btnClientMode.setVisibility(View.GONE);
        btnServerMode.setVisibility(View.GONE);
        txtModeSelection.setText(mode);
    }
}
